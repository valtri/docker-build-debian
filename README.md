# Info

Simple Debian images for build.

# Tags

Debian 12/bookworm (openjdk 17):

* *bookworm*: packaging
* *bookworm-jni*: java + maven + gcc
* *bookworm-maven*: java + maven

Debian 11/bullseye (openjdk 11):

* *bullseye*: packaging
* *bullseye-jni*: java + maven + gcc
* *bullseye-maven*: java + maven

Debian 10/buster (openjdk 11):

* *buster*: packaging
* *buster-jni*: java + maven + gcc
* *buster-maven*: java + maven

Debian 9/stretch (openjdk 8):

* *stretch*: packaging
* *stretch-dhus*: java + maven + git, hold java version 8u252
* *stretch-jni*: java + maven + gcc
* *stretch-maven*: java + maven
